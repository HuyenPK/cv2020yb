import { Component } from 'react';
import DefaultHeader from '../DefaultHeader/DefaultHeader';
import DefaultContentWrapper from '../DefaultLayout/DefaultContentWrapper';
import DefaultLayout from '../DefaultLayout/DefaultLayout';
import * as Global from '../Global/Global';

export default class About extends Component {
    themeTextColor = Global.GetThemeColor().themeTextDarkS1;

    render(){
        return(
            <DefaultLayout>
                <DefaultHeader
                    title="Persona"
                    watermark="Performance"
                    titleColor={this.themeTextColor}>
                </DefaultHeader>
                <DefaultContentWrapper>
                    <div className="about-container">
                        
                    </div>
                </DefaultContentWrapper>
            </DefaultLayout>
        );
    }
}