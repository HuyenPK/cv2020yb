import { Component } from "react";
import "./css/contact.css";
import { Connect, StartChat } from "../SignalR/Chat";
import sendIcon from "../../icons/send.png";
export default class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chatVisible: false,
      connected: false,
      messages: [
        {
          isMine: true,
          content: "Hello recruiter",
        },
        {
          isMine: false,
          content:
            "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Accusantium, fugit nulla ad delectus laudantium perspiciatis ab ex eaque distinctio sequi? Veritatis officia quis reprehenderit ipsam obcaecati ea dolorum at eligendi.",
        },
        {
          isMine: true,
          content: "Lorem ipsum dolor sit, enderit ipsam obcaecati ea dolorum at eligendi.",
        },
      ],
    };
  }
  openChat = () => {
    let name = document.getElementById("contact-name").value;
    let email = document.getElementById("contact-email").value;
    if (!this.state.connected) {
      Connect(name, email, ()=> {
        this.setState({
          ...this.state,
          connected: true,
        });
      });
      
    }
    this.setState({
      ...this.state,
      chatVisible: !this.state.chatVisible,
    });
  };

  sendMessage = () => {
    let text = document.getElementsByClassName("contact-chatbox")[0].value;
    let msg = { isMine: true, content: text };
    this.setState(
      {
        ...this.state,
        messages: [...this.state.messages, msg],
      },
      function () {
        this.scrollDown();
        document.getElementsByClassName("contact-chatbox")[0].value = "";
      }
    );
  };

  scrollDown = () => {
    let ch = document.getElementById("chat-messages");
    ch.scrollTop = ch.scrollHeight;
  };
  render() {
    return (
      <div className="contact-container">
        <div className="btn-home">
          <a href="#" title="home">
            <div className="btn-home-arrow"></div>
          </a>
        </div>
        <div className="contact-chat-container">
          <h1>Grab a coffee ?</h1>
          <div className="contact-chat-form">
            <input id="contact-name" placeholder="Your name"></input>
            <input id="contact-email" placeholder="Your email"></input>
          </div>
          <button className="contact-chat-connect" onClick={this.openChat}>
            {!this.state.chatVisible ? "Live chat" : "Hide conversation"}
          </button>
          <div id="chat-messages" className={"contact-conv " + (this.state.chatVisible ? "visible" : "hidden")}>
            {this.state.messages.map((msg, index) => {
              return (
                <div key={index} className={msg.isMine ? "contact-conv-me" : "contact-conv-them"}>
                  <p>{msg.content}</p>
                </div>
              );
            })}
          </div>
          <div className={"contact-chatbox-container " + (this.state.chatVisible ? "visible" : "hidden")}>
            <input className="contact-chatbox"></input>
            <button className="contact-send" onClick={this.sendMessage}>
              <img src={sendIcon}></img>
            </button>
          </div>
        </div>
      </div>
    );
  }
}
