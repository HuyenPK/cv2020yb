import * as signalR from "@microsoft/signalr";

async function startChatHub(name, email,callback) {
  const connection = new signalR.HubConnectionBuilder()
    .withUrl("https://chattycv.io/chathub")
    .configureLogging(signalR.LogLevel.Information)
    .build();
  try {
    await connection.start();
    console.log("Connected to chat hub.");
    var creds = await connection.invoke("Login", name, email);
    console.log("token: " + JSON.parse(creds).Token);
    callback();
  } catch (err) {
    console.log(err);
    //setTimeout(startConnection, 5000);
  }
}
function Connect(name, email,callback) {
  console.log("called");
  startChatHub(name, email,callback);
}
function StartChat() {

}
export {StartChat, Connect};