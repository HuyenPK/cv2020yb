import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
// import init_pointer from "./pointer";

ReactDOM.render(
  <React.StrictMode>
    <link rel="manifest" href="/manifest.json"></link>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

// init_pointer({
//   pointerColor: "#FFB300",
//   ringSize: 15,
//   ringClickSize: 10
// });